# ShootEmUp
[git repo](https://gitlab.com/BugProg/shoot-them-up)

Hi and welcome in our project developed with a lot a loves :) !
You will find below the modifications added to the project.

**Info :** For a better experience run the game on Linux based os.

### Feature Added

- New assets of images for the ships
- Add a scroll background
- Add enemies
- Add differents patterns of fire
- Add two differents shots for players (1p : space and B, 2p : E and R). By the way the shot placed in B and R counts like a cheat cause it one shot things
- Add player and enemy lives
- Add player immunity after being shot by an enemy (+ animation of it)
- Add player score
- Add sounds and musics
- Add text dialog
- Add possibility to get extralives (+10 each time) for players (ally -> L and enemy -> K)
- Add multiplayer mod (2p coop and 1 v 1 and the bots)
- Two waves of enemies (1st one with 3 differents shots, 2nd one with only one pattern)
- Add cmake file
- Add comments
- Add bugs
- ...
- Add readme

### How to run the game (Dev)

##### Debien/Ubuntu
``sudo apt install libsdl2-dev libsdl2-image-dev libsdl2-mixer-dev libsdl2-ttf-dev``
##### Fedora
```sudo dnf install SDL2_image SDL2_image-devel SDL2_ttf SDL2_ttf-devel```

Run with : ```gcc *.c -lm -lSDL2 -lSDL2_image -lSDL2_mixer -lSDL2_ttf && ./a.out```