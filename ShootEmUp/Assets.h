#pragma once

#include "Settings.h"

/// @brief Structure contenant l'ensemble des assets du jeu.
/// Cela correspond aux ressources utilis?es (textures, musiques, sons...)
typedef struct Assets_s
{
    /// @brief Tableau des diff?rents calques de fond.
    SDL_Texture *layers[2];

    /// @brief Texture du vaisseau des joueurs.
    SDL_Texture *player1;
    SDL_Texture *player2;
    SDL_Texture *player2Enemy;

    /// @brief Texture du tir du joueur.
    SDL_Texture *playerBullet;

    /// @brief Texture du tir du joueur.
    SDL_Texture *playerUltimateBullet;

    /// @brief Texture du tir du joueur 2 ennemi.
    SDL_Texture *player2EnemyBullet;

    /// @brief Texture du vaisseau du joueur immunisé.
    SDL_Texture *playerImmune;

    /// @brief Texture du vaisseau du joueur 2 allié immunisé.
    SDL_Texture *player2Immune;

    /// @brief Texture du vaisseau du joueur 2 ennemi immunisé.
    SDL_Texture *player2EnemyImmune;

    /// @brief Texture du vaisseau ennemi.
    SDL_Texture *fighter;

    /// @brief Texture du tir d'un ennemi.
    SDL_Texture *fighterBullet;

    int imagesIndex;

    /// @brief Texture du vaisseau ennemi 404.
    SDL_Texture *fighter404;

    /// @brief Texture du tir d'un ennemi 404.
    SDL_Texture *fighter404Bullet;

    /// @brief Texture du vaisseau ennemi minion.
    SDL_Texture *fighterMinion;

    /// @brief Texture du tir d'un ennemi minion.
    SDL_Texture *fighterMinionBullet;
} Assets;

/// @brief Cr?e la structure contenant les assets du jeu.
/// @param renderer le moteur de rendu.
/// @return La structure contenant les assets du jeu.
Assets *Assets_New(SDL_Renderer *renderer);

/// @brief D?truit la structure contenant les assets du jeu.
/// @param self les assets.
void Assets_Delete(Assets *self);
