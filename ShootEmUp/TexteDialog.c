#include "Settings.h"
#include "TexteDialog.h"

void *Dialog_init(SDL_Window *window) {
    TextDialog *self = (TextDialog *)calloc(1, sizeof(TextDialog));
    self->window = window;
}

void *Dialog_New(char title[20], char text[1000]) {
    TextDialog *self = (TextDialog *)calloc(1, sizeof(TextDialog));
    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_INFORMATION, title, text, self->window);
}
