#include "Scene.h"
#include "Text.h"
#include "TexteDialog.h"
#include "Sound.h"

Scene *Scene_New(SDL_Renderer *renderer, int numberPlayer, int multiplayerMod)
{
    Scene *self = (Scene *)calloc(1, sizeof(Scene));
    AssertNew(self);

    self->renderer = renderer;

    self->assets = Assets_New(renderer);
    self->camera = Camera_New(LOGICAL_WIDTH, LOGICAL_HEIGHT);
    self->input = Input_New();
    if (numberPlayer ==1)
    {
        self->player1 = Player_New(self, 10, 0, 1, multiplayerMod);
        self->twoPlayers = false;
    }
    else {
        self->multiplayerMod = multiplayerMod;
        self->player1 = Player_New(self, multiplayerMod == 1 ? 10 : 7, 0, 1, multiplayerMod);
        self->player2 = Player_New(self, multiplayerMod == 1 ? 5 : 7, 0, 2, multiplayerMod);
        self->twoPlayers = true;
    }
    self->waveIdx = 0;
    self->gamedEnded = false;

    return self;
}

void Scene_Delete(Scene *self)
{
    if (!self) return;

    Assets_Delete(self->assets);
    Camera_Delete(self->camera);
    Input_Delete(self->input);
    Player_Delete(self->player1);
    if (self->twoPlayers) Player_Delete(self->player2);

    for (int i = 0; i < self->enemyCount; i++)
    {
        Enemy_Delete(self->enemies[i]);
        self->enemies[i] = NULL;
    }
    for (int i = 0; i < self->bulletCount; i++)
    {
        Bullet_Delete(self->bullets[i]);
        self->bullets[i] = NULL;
    }

    free(self);
}

void Scene_UpdateLevel(Scene *self)
{
    if (self->enemyCount > 0)
        return;

    else if (self->waveIdx == 0)
    {
        Enemy *enemy = Enemy_New(self, ENEMY_FIGHTER, Vec2_Set(15.0f, 4.5f), 1);
        Scene_AppendEnemy(self, enemy);
        self->waveIdx++;
    }
    else if (self->waveIdx == 1)
    {
        playSound("boss_announcement");
        Enemy *enemy404 = Enemy_New(self, ENEMY_FIGHTER_404, Vec2_Set(15.0f, 4.5f), 1);
        Scene_AppendEnemy(self, enemy404);
        Enemy *enemy_minion_1 = Enemy_New(self, ENEMY_FIGHTER_MINION_1, Vec2_Set(13.0f, 7.5f), 3);
        Scene_AppendEnemy(self, enemy_minion_1);
        Enemy *enemy_minion_2 = Enemy_New(self, ENEMY_FIGHTER_MINION_2, Vec2_Set(13.0f, 1.5f), 3);
        Scene_AppendEnemy(self, enemy_minion_2);
        Enemy *enemy_minion_3 = Enemy_New(self, ENEMY_FIGHTER_MINION_1, Vec2_Set(8.0f, 7.5f), 3);
        Scene_AppendEnemy(self, enemy_minion_3);
        Enemy *enemy_minion_4 = Enemy_New(self, ENEMY_FIGHTER_MINION_2, Vec2_Set(8.0f, 1.5f), 3);
        Scene_AppendEnemy(self, enemy_minion_4);
        self->waveIdx++;

    }
    else if (!self->gamedEnded) {
        self->gamedEnded = true;
        playMusic("winner");
    }
}

bool Scene_Update(Scene *self)
{
    Player *player1 = self->player1;
    Player *player2;
    if (self->twoPlayers) player2 = self->player2;

    // Met � jour les entr�es utilisateur
    Input_Update(self->input);

    // -------------------------------------------------------------------------
    // Met � jour les tirs

    int i = 0;
    while (i < self->bulletCount)
    {
        Bullet *bullet = self->bullets[i];
        bool removed = false;

        Bullet_Update(bullet);

        bool outOfBounds =
            (bullet->position.x < -1.0f) ||
            (bullet->position.x > 17.0f) ||
            (bullet->position.y < -1.0f) ||
            (bullet->position.y > 10.0f);

        if (outOfBounds)
        {
            // Supprime le tir
            Scene_RemoveBullet(self, i);
            removed = true;
            continue;
        }

        if (bullet->fromPlayer)
        {
            // Teste les collisions avec les ennemis
            for (int j = 0; j < self->enemyCount; j++)
            {
                Enemy *enemy = self->enemies[j];
                float dist = Vec2_Distance(bullet->position, enemy->position);
                if ((dist < bullet->radius + enemy->radius)&& bullet->type != BULLET_ULTIMATE_PLAYER)
                {
                    // Inflige des dommages � l'ennemi
                    Enemy_Damage(enemy, 1);

                    // Supprime le tir
                    Scene_RemoveBullet(self, i);
                    removed = true;
                    break;
                }
                if ((dist < bullet->radius + enemy->radius) && bullet->type == BULLET_ULTIMATE_PLAYER)
                {
                    // Inflige des dommages � l'ennemi
                    Enemy_Damage(enemy, 1000);

                    // Supprime le tir
                    Scene_RemoveBullet(self, i);
                    removed = true;
                    break;
                }
            }
            if (self->multiplayerMod == 1) {
                float dist = Vec2_Distance(bullet->position, player2->position);
                if (dist < bullet->radius + player2->radius)
                {
                    // Inflige des dommages � l'ennemi
                    Player_Damage(player2, 1);

                    // Supprime le tir
                    Scene_RemoveBullet(self, i);
                    removed = true;
                    break;
                }
            }
        }
        else
        {
            // Teste la collision avec le joueur
            float dist = Vec2_Distance(bullet->position, self->player1->position);
            if (dist < bullet->radius + player1->radius)
            {
                // Inflige des dommages au joueur
                Player_Damage(player1, 1);

                // Supprime le tir
                Scene_RemoveBullet(self, i);
                removed = true;
            }

            if (self->twoPlayers) {
                //             Teste la collision avec le joueur
                float dist1 = Vec2_Distance(bullet->position, self->player2->position);
                if (dist1 < bullet->radius + player2->radius)
                {
                    if (bullet->type == BULLET_FIGHTER_PLAYER && self->multiplayerMod == 1) {
                        break;
                    }
                    // Inflige des dommages au joueur
                    Player_Damage(player2, 1);

                    // Supprime le tir
                    Scene_RemoveBullet(self, i);
                    removed = true;
                }
            }
            }



        // Passe au prochain tir
        if (removed == false)
        {
            i++;
        }
    }

    // -------------------------------------------------------------------------
    // Met � jour les ennemis

    i = 0;
    while (i < self->enemyCount)
    {
        Enemy *enemy = self->enemies[i];
        bool removed = false;

        Enemy_Update(enemy);

        if (enemy->state == ENEMY_DEAD)
        {
            // Supprime l'ennemi
            Scene_RemoveEnemy(self, i);
            removed = true;
        }

        // Passe au prochain ennemi
        if (removed == false)
        {
            i++;
        }
    }

    // -------------------------------------------------------------------------
    // Met � jour le joueur
    Player_Update(self->player1);
    if (self->twoPlayers) Player_Update(self->player2);

    // -------------------------------------------------------------------------
    // Met � jour le niveau

    Scene_UpdateLevel(self);

    return self->input->quitPressed;
}

void Scene_Render(Scene *self)
{
    // Affichage du fond
    SDL_Renderer *renderer = Scene_GetRenderer(self);
    Assets *assets = Scene_GetAssets(self);

    SDL_Rect dstrect_layer1;
    dstrect_layer1.x = -(self->assets->imagesIndex++ / 2 % 1200);;
    dstrect_layer1.y = 0;
    dstrect_layer1.w = 3840;
    dstrect_layer1.h = 1080;
    SDL_Rect dstrect_layer0;
    dstrect_layer0.x = -(self->assets->imagesIndex++ / 10 % 700);;
    dstrect_layer0.y = 0;
    dstrect_layer0.w = 3840;
    dstrect_layer0.h = 1080;
    SDL_RenderCopy(renderer, assets->layers[1], NULL, &dstrect_layer1);
    SDL_RenderCopy(renderer, assets->layers[0], NULL, &dstrect_layer0);

    // Affichage des bullets
    int bulletCount = self->bulletCount;
    for (int i = 0; i < bulletCount; i++)
    {
        Bullet_Render(self->bullets[i]);
    }

    // Display texts

    // Lives player 1
    char player1Lives[10];
    char lives[20] = "Lives Player 1 : ";
    sprintf(player1Lives, "%d", self->player1->lives);
    strcat(lives, player1Lives);
    Text_New(
            self,
            lives,
            0,
            650,
            200,
            50,
            70,
            255, self->player1->lives <= 1 ? 0 : 255, self->player1->lives <= 1 ? 0 : 255);

    // Lives player 2
    if (self->twoPlayers) {
        char playerLives[10];
        char lives_player2[20] = "Lives Player 2 : ";
        sprintf(playerLives, "%d", self->player2->lives);

        strcat(lives_player2, playerLives);
        Text_New(
                self,
                lives_player2,
                300,
                650,
                200,
                50,
                70,
                255, self->player2->lives <= 1 ? 0 : 255, self->player2->lives <= 1 ? 0 : 255);
    }

    // Score players
    char playerScore[10];
    char scores[20] = "Score : ";
    int score = self->player1->score;
    if (self->twoPlayers) score += self->player2->score;
    sprintf(playerScore, "%d", score);
    strcat(scores, playerScore);
    Text_New(self, scores, 800, 650, 200, 50, 70, 255, 255, 255);

    if (self->gamedEnded) {
        Text_New(self,
                 "Great ! You saved the universe from the Empire.\n Now you can live in a peaceful world !",
                 150,
                 100,
                 1080,
                 70,
                 50,
                 255, 255, 255);
        Text_New(self,
                 "This wonderful game was developed by \n Alystan and Adrian \n",
                 150,
                 150,
                 1080,
                 70,
                 50,
                 255, 255, 255);
        Text_New(self,
                 "With the help of: M.Bannier & M.Bodin",
                 150,
                 200,
                 1080,
                 70,
                 50,
                 255, 255, 255);
        Text_New(self,
                 "PRESS ECHAP TO EXIT",
                 150,
                 300,
                 600,
                 70,
                 50,
                 255, 255, 255);
    }

    // Display enemies
    int enemyCount = self->enemyCount;
    for (int i = 0; i < enemyCount; i++)
    {
        Enemy_Render(self->enemies[i]);
    }

    // Display Players

    // Show text if the players are dead
    if ((!self->twoPlayers && (self->player1->state == PLAYER_DEAD && self->player1->stateMemory != PLAYER_DEAD))
        ||
        (self->twoPlayers && (self->player2->state == PLAYER_DEAD && self->player1->state == PLAYER_DEAD &&
        self->player1->stateMemory != PLAYER_DEAD))) {
        playMusic("loosing");
        Dialog_New("Try again !",
                   "OOOOOOOHhhh non !\n"
                   "L'Empire a gagné :(\n\n"
                   "Privés de leader, les peuples de la Galaxie n'ont pu se liberer du Mal !\n"
                   "Une privation sévère des libertés a été promulguée...\n"
                   "...Mais un jour la lumiere de l'OpenSource renaitra de ses cendres !");
        exit(0);
    }
    if ((self->multiplayerMod == 1)&&(self->twoPlayers && (self->player2->state != PLAYER_DEAD && self->player1->state == PLAYER_DEAD &&
        self->player1->stateMemory != PLAYER_DEAD))) {
        playMusic("loosing");
        Dialog_New("Traitre !",
                   "Vous avez rejoint le côté obscur de la Force !\n"
                   "L'Empire a gagné :(\n\n"
                   "Privés de leader, les peuples de la Galaxie n'ont pu se liberer du Mal !\n"
                   "Une privation sévère des libertés a été promulguée, sur vous y compris, ...\n"
                   "...Mais un jour la lumiere de l'OpenSource renaitra de ses cendres !");
        exit(0);
    }

    if ((Timer_GetElapsed(g_time) - self->player1->livesTimeMemory) > 2 && self->player1->state == PLAYER_IMMUNITY) {
        printf("Flying !!\n");
        self->player1->state = PLAYER_FLYING;
    }

    if (self->player1->state == PLAYER_IMMUNITY) {
        self->player1->texture = self->assets->playerImmune;
    } else {
        self->player1->texture = self->assets->player1;
    }

    if (self->twoPlayers) {
        if ((Timer_GetElapsed(g_time) - self->player2->livesTimeMemory) > 2 && self->player2->state == PLAYER_IMMUNITY) {
            printf("Flying !!\n");
            self->player2->state = PLAYER_FLYING;
        }

        if (self->player2->state == PLAYER_IMMUNITY) {
            if (self->player2->multiplayerMod == 0) {
                self->player2->texture = self->assets->player2Immune;
                } else {
                self->player2->texture = self->assets->player2EnemyImmune; }
        } else {
            if (self->player2->multiplayerMod == 0) {
                self->player2->texture = self->assets->player2;
            } else {
                self->player2->texture = self->assets->player2Enemy;
            }
        }
    }


    Player_Render(self->player1);
    if(self->twoPlayers) Player_Render(self->player2);
}

void Scene_AppendObject(void *object, void **objectArray, int *count, int capacity)
{
    int index = *count;
    if (index >= capacity)
        return;

    if (objectArray[index] != NULL)
    {
        assert(false);
    }

    objectArray[index] = object;
    (*count)++;
}

void Scene_AppendEnemy(Scene *self, Enemy *enemy)
{
    Scene_AppendObject(
        enemy,
        (void **)(self->enemies),
        &(self->enemyCount),
        ENEMY_CAPACITY
    );
}

void Scene_AppendBullet(Scene *self, Bullet *bullet)
{
    Scene_AppendObject(
        bullet,
        (void **)(self->bullets),
        &(self->bulletCount),
        BULLET_CAPACITY
    );
}

void Scene_RemoveObject(int index, void **objectArray, int *count)
{
    int lastIndex = *count - 1;
    assert(0 <= index && index < *count);

    if (objectArray[index] == NULL)
    {
        assert(false);
        abort();
    }

    if (index == lastIndex)
    {
        // Supprime le pointeur
        objectArray[index] = NULL;
    }
    else
    {
        // Remplace le pointeur par le dernier du tableau
        objectArray[index] = objectArray[lastIndex];
        objectArray[lastIndex] = NULL;
    }
    (*count)--;
}

void Scene_RemoveEnemy(Scene *self, int index)
{
    self->player1->score += 100;
    if (self->twoPlayers) self->player2->score += 100;
    Enemy_Delete(self->enemies[index]);
    Scene_RemoveObject(index, (void **)(self->enemies), &(self->enemyCount));
}

void Scene_RemoveBullet(Scene *self, int index)
{
    Bullet_Delete(self->bullets[index]);
    Scene_RemoveObject(index, (void **)(self->bullets), &(self->bulletCount));
}
