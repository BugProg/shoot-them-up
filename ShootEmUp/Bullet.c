#include "Bullet.h"
#include "Common.h"
#include "Scene.h"

Bullet *Bullet_New(Scene *scene, Vec2 position, Vec2 velocity, int type, float angle)
{
    Bullet *self = (Bullet *)calloc(1, sizeof(Bullet));
    AssertNew(self);

    self->position = position;
    self->velocity = velocity;
    self->type = type;
    self->angle = angle;
    self->scene = scene;
    self->fromPlayer = false;

    Assets *assets = Scene_GetAssets(scene);
    switch (type)
    {
    case BULLET_FIGHTER:
        self->texture = assets->fighterBullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = false;
        break;
    case BULLET_FIGHTER_404:
        self->texture = assets->fighter404Bullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = false;
        break;
    case BULLET_FIGHTER_MINION:
        self->texture = assets->fighterMinionBullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = false;
        break;
    case BULLET_FIGHTER_PLAYER:
        self->texture = assets->player2EnemyBullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = false;
        break;

    case BULLET_FIGHTER_ULTIMATE_PLAYER:
        self->texture = assets->playerUltimateBullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = false;
        break;
    
    case BULLET_ULTIMATE_PLAYER:
        self->texture = assets->playerUltimateBullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = true;
        break;

    default:
    case BULLET_PLAYER:
        self->texture = assets->playerBullet;
        self->worldW = 8 * PIX_TO_WORLD;
        self->worldH = 16 * PIX_TO_WORLD;
        self->radius = 0.05f;
        self->fromPlayer = true;
        break;
    }

    return self;
}

void Bullet_Delete(Bullet *self)
{
    if (!self) return;
    free(self);
}

void Bullet_Update(Bullet *self)
{
//    Assets *assets = Scene_GetAssets(self);
// Mise à jour de la vitesse en fonction de l'état des touches
// Mise à jour de la position
    self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
            self->position, // (vitesse * temps écoulé)
            Vec2_Scale(self->velocity, Timer_GetDelta(g_time)));
}

void Bullet_Render(Bullet *self)
{
    // On récupère des infos essentielles (communes à tout objet)
    Scene *scene = self->scene;
    SDL_Renderer *renderer = Scene_GetRenderer(scene);
    Camera *camera = Scene_GetCamera(scene);
// On calcule la position en pixels en fonction de la position
// en tuiles, la taille de la fenêtre et la taille des textures.
    float scale = Camera_GetWorldToViewScale(camera);
    SDL_FRect dst = { 0 };
// Changez 48 par une autre valeur pour grossir ou réduire l'objet
    dst.h = 12 * PIX_TO_WORLD * scale;
    dst.w = 12 * PIX_TO_WORLD * scale;
    Camera_WorldToView(camera, self->position, &dst.x, &dst.y);
// Le point de référence est le centre de l'objet
    dst.x -= 0.50f * dst.w;
    dst.y -= 0.50f * dst.h;
// On affiche en position dst (unités en pixels)
    SDL_RenderCopyExF(
            renderer, self->texture, NULL, &dst, 90.0f, NULL, 0);
    if(self->type == BULLET_ULTIMATE_PLAYER)
    {
        dst.h = 10 * PIX_TO_WORLD * scale;
        dst.w = 30 * PIX_TO_WORLD * scale;
        SDL_RenderCopyExF(
            renderer, self->texture, NULL, &dst, 180.0f, NULL, 0);
    }
    if(self->type == BULLET_FIGHTER_ULTIMATE_PLAYER)
    {
        dst.h = 10 * PIX_TO_WORLD * scale;
        dst.w = 30 * PIX_TO_WORLD * scale;
        SDL_RenderCopyExF(
            renderer, self->texture, NULL, &dst, 0.0f, NULL, 0);
    }
}