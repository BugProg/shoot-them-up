#pragma once

#include "Settings.h"
#include "Scene.h"

typedef struct Scene_s Scene;

/// @brief Add text to screen
/// @param scene The scene
/// @param message The message
/// @param x X position
/// @param y Y position
/// @param rectWidth Rectangle width
/// @param rectHeight Rectangle height
/// @param fontSize The font size
/// @param r red color [0-255]
/// @param g green color [0-255]
/// @param r red color [0-255]
void Text_New (Scene *scene, char message[20], int x, int y , int rectWidth, int rectHeight, int fontSize, int r, int g, int b);
