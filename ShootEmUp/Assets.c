#include "Assets.h"

typedef struct TextureSpec_s
{
    SDL_Texture **ptr;
    char *path;
} TextureSpec;

Assets *Assets_New(SDL_Renderer *renderer)
{
    Assets *self = (Assets *)calloc(1, sizeof(Assets));
    AssertNew(self);

    self->imagesIndex = 0;

    // -------------------------------------------------------------------------
    // Chargement des textures

    TextureSpec texSpecs[] = {
        { &self->layers[0],     "../Assets/Background/layer_03.png"   },
        { &self->layers[1],     "../Assets/Background/layer_00.png"   },
        { &self->playerBullet,  "../Assets/Player/bullet_default.png" },
        { &self->playerUltimateBullet,  "../Assets/Player/bullet_ultimate.png" },
        { &self->player2EnemyBullet,  "../Assets/Player/bullet_default_enemy.png" },
        { &self->fighter,       "../Assets/Enemy/fighter.png"         },
        { &self->fighterBullet, "../Assets/Enemy/fighter_bullet.png"  },
        { &self->fighter404,       "../Assets/Enemy/fighter_404.png"         },
        { &self->fighter404Bullet, "../Assets/Enemy/fighter_404_bullet.png"  },
        { &self->fighterMinion,       "../Assets/Enemy/fighter_minion.png"         },
        { &self->fighterMinionBullet, "../Assets/Enemy/fighter_minion_bullet.png"  },
        { &self->player1,        "../Assets/Player/player.png"},
        { &self->player2,        "../Assets/Player/player2.png"},
        { &self->player2Enemy,        "../Assets/Player/player2_enemy.png"},
        { &self->playerImmune,        "../Assets/Player/player_immune.png"},
        { &self->player2Immune,        "../Assets/Player/player2_immune.png"},
        { &self->player2EnemyImmune,        "../Assets/Player/player2_enemy_immune.png"}
        
    };
    int texSpecCount = sizeof(texSpecs) / sizeof(TextureSpec);

    for (int i = 0; i < texSpecCount; i++)
    {
        SDL_Texture **texPtr = texSpecs[i].ptr;
        char *path = texSpecs[i].path;

        *texPtr = IMG_LoadTexture(renderer, path);
        if (*texPtr == NULL)
        {
            printf("ERROR - Loading texture %s\n", path);
            printf("      - %s\n", SDL_GetError());
            assert(false);
            abort();
        }
    }

    return self;
}

void Assets_Delete(Assets *self)
{
    if (!self) return;

    // -------------------------------------------------------------------------
    // Lib�re les textures

    SDL_Texture **texPointers[] = {
        &self->layers[0],
        &self->layers[1],
        &self->playerBullet,
        &self->playerUltimateBullet,
        &self->player2EnemyBullet,
        &self->fighter,
        &self->fighterBullet,
        &self->fighter404,
        &self->fighter404Bullet,
        &self->fighterMinion,
        &self->fighterMinionBullet,
        &self->player1,
        &self->player2,
        &self->player2Enemy,
        &self->playerImmune,
        &self->player2Immune,
        &self->player2EnemyImmune,
    };
    int count = sizeof(texPointers) / sizeof(SDL_Texture **);

    for (int i = 0; i < count; i++)
    {
        if (*texPointers[i])
            SDL_DestroyTexture(*(texPointers[i]));
    }

    free(self);
}