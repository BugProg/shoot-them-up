#include "Assets.h"

void playMusic(char title[]) {
    char filePath[50];
    if (strcmp(title, "loosing") == 0) {
        strcpy(filePath, "../Assets/Sounds/loosing.wav");
    } else if (strcmp(title, "playing") == 0) {
        strcpy(filePath, "../Assets/Sounds/playing.wav");
    } else if (strcmp(title, "winner") == 0) {
        strcpy(filePath, "../Assets/Sounds/winner.wav");
    }

    if (!strcmp(filePath, "")) {
        return;
    }

    Mix_Music * music = Mix_LoadMUS(filePath);
    Mix_PlayMusic(music, -1);
}

void playSound(char title[]) {

    char filePath[50] = "";
    if (strcmp(title, "shoot") == 0) {
        strcpy(filePath, "../Assets/Sounds/shoot.wav");
    } else if (strcmp(title, "touched") == 0) {
        strcpy(filePath, "../Assets/Sounds/touched.wav");
    } else if (strcmp(title, "destroyed") == 0) {
        strcpy(filePath, "../Assets/Sounds/destroyed.wav");
    } else if (strcmp(title, "immunity") == 0) {
        strcpy(filePath, "../Assets/Sounds/immunityActivated.wav");
    } else if (strcmp(title, "boss_announcement") == 0) {
        strcpy(filePath, "../Assets/Sounds/boss.wav");
    } else if (strcmp(title, "big_shoot") == 0) {
        strcpy(filePath, "../Assets/Sounds/big_shoot.wav");
    } else if (strcmp(title, "reload") == 0) {
        strcpy(filePath, "../Assets/Sounds/reload.wav");
    }

    if (!strcmp(filePath, "")) {
        return;
    }

    Mix_Chunk * sound  = Mix_LoadWAV(filePath);
    Mix_PlayChannel(-1, sound, 0);
}

void stopMusics() {
    Mix_HaltMusic();
}