#include "Input.h"
#include "Common.h"

Input *Input_New() {
    Input *self = (Input *) calloc(1, sizeof(Input));
    AssertNew(self);

    return self;
}

void Input_Delete(Input *self) {
    if (!self) return;
    free(self);
}

void Input_Update(Input *self) {
    self->quitPressed = false;

    SDL_Event evt;
    while (SDL_PollEvent(&evt)) {
        switch (evt.type) {
            case SDL_QUIT:
                self->quitPressed = true;
                break;

            case SDL_KEYDOWN:
                if (evt.key.repeat)
                    break;

                switch (evt.key.keysym.scancode) {
                    case SDL_SCANCODE_ESCAPE:
                        self->quitPressed = true;
                        break;

//                    PLAYER 1
                    case SDL_SCANCODE_RIGHT:
                        // Deplacement � droite
                        self->hAxis = 1.f;
                        break;

                    case SDL_SCANCODE_LEFT:
                        // Deplacement � gauche
                        self->hAxis = -1.f;
                        break;

                    case SDL_SCANCODE_UP:
                        // Deplacement en haut
                        self->vAxis = 1.f;
                        break;

                    case SDL_SCANCODE_DOWN:
                        // Deplacement en bas
                        self->vAxis = -1.f;
                        break;

                    case SDL_SCANCODE_L :
                        self->livesPressed = true;
                        break;

//                    PLAYER 2
                    case SDL_SCANCODE_D:
                        // Deplacement � droite
                        self->hAxis2 = 1.f;
                        break;

                    case SDL_SCANCODE_A:
                        // Deplacement � gauche
                        self->hAxis2 = -1.f;
                        break;

                    case SDL_SCANCODE_W:
                        // Deplacement en haut
                        self->vAxis2 = 1.f;
                        break;

                    case SDL_SCANCODE_S:
                        // Deplacement en bas
                        self->vAxis2 = -1.f;
                        break;

                    case SDL_SCANCODE_K:
                        self->livesEnemyPressed = true;
                        break;

                    case SDL_SCANCODE_SPACE:
                        self->shootPressed = true;
                        break;

                    case SDL_SCANCODE_B:
                        self->shootUltimatePressed = true;
                        break;

                    case SDL_SCANCODE_E:
                        self->shootPressed_Player2 = true;
                        break;

                    case SDL_SCANCODE_R:
                        self->shootUltimatePressed_Player2 = true;
                        break;

                    default:
                        break;
                }
                break;

            case SDL_KEYUP:
                if (evt.key.repeat)
                    break;

                switch (evt.key.keysym.scancode) {
//                PLAYER 1
                    case SDL_SCANCODE_RIGHT:
                        // Deplacement � droite
                        if (self->hAxis > 0.f)
                            self->hAxis = 0.f;
                        break;

                    case SDL_SCANCODE_LEFT:
                        // Deplacement � gauche
                        if (self->hAxis < 0.f)
                            self->hAxis = 0.f;
                        break;

                    case SDL_SCANCODE_UP:
                        // Deplacement en haut
                        if (self->vAxis > 0.f)
                            self->vAxis = 0.f;
                        break;

                    case SDL_SCANCODE_DOWN:
                        // Deplacement en bas
                        if (self->vAxis < 0.f)
                            self->vAxis = 0.f;
                        break;
                    case SDL_SCANCODE_L:
                        self->livesPressed = false;
                        break;

//                    PLAYER 2
                    case SDL_SCANCODE_D:
                        // Deplacement � droite
                        if (self->hAxis2 > 0.f)
                            self->hAxis2 = 0.f;
                        break;

                    case SDL_SCANCODE_A:
                        // Deplacement � gauche
                        if (self->hAxis2 < 0.f)
                            self->hAxis2 = 0.f;
                        break;

                    case SDL_SCANCODE_W:
                        // Deplacement en haut
                        if (self->vAxis2 > 0.f)
                            self->vAxis2 = 0.f;
                        break;

                    case SDL_SCANCODE_S:
                        // Deplacement en bas
                        if (self->vAxis2 < 0.f)
                            self->vAxis2 = 0.f;
                        break;

                    case SDL_SCANCODE_K:
                        self->livesEnemyPressed = false;
                        break;
                    
                    case SDL_SCANCODE_SPACE:
                        self->shootPressed = false;
                        break;

                    case SDL_SCANCODE_E:
                        self->shootPressed_Player2 = false;
                        break;
                    
                    case SDL_SCANCODE_B:
                        self->shootUltimatePressed = false;
                        break;

                    case SDL_SCANCODE_R:
                        self->shootUltimatePressed_Player2 = false;
                        break;

                    default:
                        break;
                }
                break;
        }
    }
}
