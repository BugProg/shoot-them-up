
#include "Settings.h"
#include "Common.h"
#include "Timer.h"
#include "Scene.h"
#include "TexteDialog.h"

void playerInit(int *numberPlayer, int *multiplayerMod, SDL_Window *window);

int main(int argc, char *argv[])
{
    //--------------------------------------------------------------------------
    // Initialisation

    // Initialise la SDL
    Game_Init(SDL_INIT_VIDEO, IMG_INIT_PNG);


    // Create window
    int sdlFlags = 0;
#ifdef FULLSCREEN
    sdlFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
#endif
    SDL_Window *window = SDL_CreateWindow(
        u8"Shoot'Em Up", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        WINDOW_WIDTH, WINDOW_HEIGHT, sdlFlags
    );
    SDL_HideWindow(window);
    if (!window)
    {
        printf("ERROR - Create window %s\n", SDL_GetError());
        assert(false); abort();
    }

    int  numberPlayer = 0; // Max 2
    int  multiplayerMod = 0; // 0: ally 1: enemy
    playerInit(&numberPlayer, &multiplayerMod, window);

    // Cr?e le moteur de rendu
    SDL_Renderer * renderer = SDL_CreateRenderer(
        window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
    );

    SDL_RenderSetLogicalSize(renderer, LOGICAL_WIDTH, LOGICAL_HEIGHT);
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "0");

    SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

    if (!renderer)
    {
        printf("ERROR - Create renderer %s\n", SDL_GetError());
        assert(false); abort();
    }


    // Cr?e le temps global du jeu
    g_time = Timer_New();
    AssertNew(g_time);

    //--------------------------------------------------------------------------
    // Boucle de rendu

    Scene *scene = Scene_New(renderer, numberPlayer, multiplayerMod);
    SDL_ShowWindow(window);
    while (true)
    {
        // Met � jour le temps
        Timer_Update(g_time);

        // Met � jour la sc?ne
        bool quitLoop = Scene_Update(scene);
        if (quitLoop)
            break;

        // Efface le rendu pr?c?dent
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        // Dessine la sc?ne
        Scene_Render(scene);

        // Affiche le nouveau rendu
        SDL_RenderPresent(renderer);
    }

    //--------------------------------------------------------------------------
    // Lib?ration de la m?moire

    Scene_Delete(scene);
    scene = NULL;

    Timer_Delete(g_time);
    g_time = NULL;

    SDL_DestroyRenderer(renderer);
    renderer = NULL;

    SDL_DestroyWindow(window);
    window = NULL;

    Game_Quit();

    return EXIT_SUCCESS;
}

void playerInit(int *numberPlayer, int *multiplayerMod, SDL_Window *window) {
    char name[10];
    char finalString[1000] = "";
    printf("\e[1;1H\e[2J"); // Clear the console
    printf("Enter your squad name to start the game : ");
    scanf("%s", name);
    printf("Number of players [1 or 2] : ");
    scanf("%d", numberPlayer);
    if (*numberPlayer > 2 || *numberPlayer < 1) {
        printf("Error number of player Invalid");
        exit(1);
    }
    if (*numberPlayer == 2) {
        printf("Multiplayer mod : ally -> 0 or enemy -> 1 : ");
        scanf("%d", multiplayerMod);
        if (*multiplayerMod > 1 || numberPlayer < 0) {
            printf("Error mod number is Invalid");
            exit(1);
        }
    }

    strcat(finalString, "Bonjour ");
    strcat(finalString, name);
    strcat(finalString, " !\n");
    strncat(finalString,
            "L'heure est grave ! L'Empire du Mal pervertit les moeurs de nos concitoyens !\n"
            "Mais la Resistance s'organise ! Deviens la lueur qui rapportera la paix et la prosperite dans toute la galaxie !",
            1000 - strlen(finalString) -1);

    Dialog_init(window);
    Dialog_New("L'Appel du Devoir",finalString);
}