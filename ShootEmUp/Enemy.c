#include "Enemy.h"
#include "Common.h"
#include "Scene.h"
#include "Sound.h"

Enemy *Enemy_New(Scene *scene, int type, Vec2 position, int lives)
{
    Enemy *self = (Enemy *)calloc(1, sizeof(Enemy));
    AssertNew(self);

    self->scene = scene;
    self->position = position;
    self->type = type;
    self->lives = lives;
    self->state = ENEMY_FIRING;
    self->speed_enemy = 1;

    Assets *assets = Scene_GetAssets(self->scene);
    switch (type)
    {
    case ENEMY_FIGHTER:
        self->worldW = 64 * PIX_TO_WORLD;
        self->worldH = 64 * PIX_TO_WORLD;
        self->radius = 0.4f;
        self->texture = assets->fighter;
        break;
    case ENEMY_FIGHTER_404:
        self->worldW = 64 * PIX_TO_WORLD;
        self->worldH = 64 * PIX_TO_WORLD;
        self->radius = 0.4f;
        self->texture = assets->fighter404;
        break;
    case ENEMY_FIGHTER_MINION_1:
        self->worldW = 64 * PIX_TO_WORLD;
        self->worldH = 64 * PIX_TO_WORLD;
        self->radius = 0.4f;
        self->texture = assets->fighterMinion;
        break;
    case ENEMY_FIGHTER_MINION_2:
        self->worldW = 64 * PIX_TO_WORLD;
        self->worldH = 64 * PIX_TO_WORLD;
        self->radius = 0.4f;
        self->texture = assets->fighterMinion;
        break;

    default:
        assert(false);
    }

    return self;
}

void Enemy_Delete(Enemy *self)
{
    if (!self) return;
    free(self);
}

void Enemy_Update(Enemy *self)
{
    switch (self->type)
    {
    case ENEMY_FIGHTER:
    if ((Timer_GetElapsed(g_time) - self->Timer_E1_M1 >=1)&&(Timer_GetElapsed(g_time) - self->Timer_E1_M1 < 3))
    {
    Vec2 velocity = Vec2_Set(0.0f, 1.0f);
    self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
    self->position, // (vitesse * temps écoulé)
    Vec2_Scale(velocity, Timer_GetDelta(g_time)));
    }
    else if ((Timer_GetElapsed(g_time) - self->Timer_E1_M1 >= 0)&&(Timer_GetElapsed(g_time) - self->Timer_E1_M1 < 1))
    {
        Vec2 velocity = Vec2_Set(0.0f, -1.0f);
        self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
        self->position, // (vitesse * temps écoulé)
        Vec2_Scale(velocity, Timer_GetDelta(g_time)));
    }
    else if ((Timer_GetElapsed(g_time) - self->Timer_E1_M1 >= 3)&&(Timer_GetElapsed(g_time) - self->Timer_E1_M1 < 4))
    {
        Vec2 velocity = Vec2_Set(0.0f, -1.0f);
        self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
        self->position, // (vitesse * temps écoulé)
        Vec2_Scale(velocity, Timer_GetDelta(g_time)));
    }
    else
    {
        self->Timer_E1_M1 = (int)Timer_GetElapsed(g_time);
    }
    if ((Timer_GetElapsed(g_time) - self->Timer_E1_S1 > 1)&&(Timer_GetElapsed(g_time) - self->Timer_E1_S3 < 25))
    {
        for (float i = -2.0f ; i < 3 ; i++)
        {
            Vec2 velocity = Vec2_Set(-4.4f*self->speed_enemy, i);
            Bullet *bullet = Bullet_New(
                    self->scene, self->position, velocity, BULLET_FIGHTER, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E1_S1 = Timer_GetElapsed(g_time);
        }
    }

    if ((Timer_GetElapsed(g_time) - self->Timer_E1_S2 >= 4)&&(Timer_GetElapsed(g_time) - self->Timer_E1_S3 >= 10)&&(Timer_GetElapsed(g_time) - self->Timer_E1_S3 < 23))
    {
        for (float i = -3.0f ; i < 3.1 ; i = i + 0.3f)
        {
            Vec2 velocity = Vec2_Set(-3.4f*self->speed_enemy, i);
            Bullet *bullet = Bullet_New(
                    self->scene, self->position, velocity, BULLET_FIGHTER, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E1_S2 = Timer_GetElapsed(g_time);
        }
    }
    if ((Timer_GetElapsed(g_time) - self->Timer_E1_S3 > 25))
    {
            for (int j = 0 ; j < 3 ; j++)
            {
                for (float i = -40.0f ; i < 40.1f ; i += 40.0f)
                {
                    Vec2 velocity = Vec2_Set(-80.0f, i);
                    Bullet *bullet = Bullet_New(
                            self->scene, self->position, velocity, BULLET_FIGHTER, 0.0f);
                    Scene_AppendBullet(self->scene, bullet);
                }
            }
            if (Timer_GetElapsed(g_time) - self->Timer_E1_S3 > 34)
            {
                self->Timer_E1_S3 = Timer_GetElapsed(g_time);
                self->speed_enemy *= 2;
            }
    }
    if ((Timer_GetElapsed(g_time) - self->Timer_E1_S4 >= 1 || Timer_GetElapsed(g_time) - self->Timer_E1_S5 >= 1.33f || Timer_GetElapsed(g_time) - self->Timer_E1_S6 >= 1.66f)&&(Timer_GetElapsed(g_time) - self->Timer_E1_S3 < 26))
    {
            for (float i = -3.0f ; i < 3.1f ; i += 2.0f) {
                Vec2 velocity = Vec2_Set(-2.0f*self->speed_enemy, cos(sin(Timer_GetElapsed(g_time)))*i);
                Bullet *bullet1 = Bullet_New(
                        self->scene, self->position, velocity, BULLET_FIGHTER, 0.0f);
                Scene_AppendBullet(self->scene, bullet1);
                if (Timer_GetElapsed(g_time) - self->Timer_E1_S4 >= 1)
                { self->Timer_E1_S4 = Timer_GetElapsed(g_time);
                } else if (Timer_GetElapsed(g_time) - self->Timer_E1_S5 >= 1)
                { self->Timer_E1_S5 = Timer_GetElapsed(g_time);
                } else { self->Timer_E1_S6 = Timer_GetElapsed(g_time); } 
            }
    }
    break;
    case ENEMY_FIGHTER_404:
    if (Timer_GetElapsed(g_time) - self->Timer_E1_S1 > 1)
    {
        for (float i = -1 ; i < 1.1f ; i+=2)
        {
            for(float j = 1 ; j < 2.1f ; j++)
            {
            Vec2 velocity = Vec2_Set(-1.0f*j, sin(Timer_GetElapsed(g_time)*i));
            Bullet *bullet = Bullet_New(
                self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
                Scene_AppendBullet(self->scene, bullet);
            self->Timer_E1_S1 = Timer_GetElapsed(g_time);
            }
        }
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E1_S2 > 1.33)
    {
        for (float i = -1 ; i < 1.1f ; i++)
        {
            for(float j = 1 ; j < 2.1f ; j++)
            {
            Vec2 velocity = Vec2_Set(-1.0f*j, sin(Timer_GetElapsed(g_time)*i));
            Bullet *bullet = Bullet_New(
                self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
                Scene_AppendBullet(self->scene, bullet);
            self->Timer_E1_S2 = Timer_GetElapsed(g_time);
            }
        }
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E1_S3 > 1.66)
    {
        for (float i = -1 ; i < 1.1f ; i++)
        {
            for(float j = 1 ; j < 2.1f ; j++)
            {
            Vec2 velocity = Vec2_Set(-1.0f*j, sin(Timer_GetElapsed(g_time)*i));
            Bullet *bullet = Bullet_New(
                self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
                Scene_AppendBullet(self->scene, bullet);
            self->Timer_E1_S3 = Timer_GetElapsed(g_time);
            }
        }
    }
    break;
    case ENEMY_FIGHTER_MINION_1:
    if (Timer_GetElapsed(g_time) - self->Timer_E3_S1 > 1)
    {
        for (float i = -4.0f ; i < 0.1f ; i += 2)
        {
            Vec2 velocity = Vec2_Set(-3.0f, i);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E3_S1 = Timer_GetElapsed(g_time);
        }            
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E3_S2 > 1.33f)
    {
        for (float i = -4.0f ; i < 0.1f ; i += 2)
        {
            Vec2 velocity = Vec2_Set(-3.0f, i);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E3_S2 = Timer_GetElapsed(g_time);
        }            
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E3_S3 > 1.66f)
    {
        for (float i = -4.0f ; i < 0.1f ; i += 2)
        {
            Vec2 velocity = Vec2_Set(-3.0f, i);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E3_S3 = Timer_GetElapsed(g_time);
        }            
    }
    case ENEMY_FIGHTER_MINION_2:
    if (Timer_GetElapsed(g_time) - self->Timer_E3_S1 > 1)
    {
        for (float i = -0.0f ; i < 4.1f ; i += 2)
        {
            Vec2 velocity = Vec2_Set(-3.0f, i);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E3_S1 = Timer_GetElapsed(g_time);
        }            
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E3_S2 > 1.33f)
    {
        for (float i = -0.0f ; i < 4.1f ; i += 2)
        {
            Vec2 velocity = Vec2_Set(-3.0f, i);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E3_S2 = Timer_GetElapsed(g_time);
        }            
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E3_S3 > 1.66f)
    {
        for (float i = 0.0f ; i < 4.1f ; i += 2)
        {
            Vec2 velocity = Vec2_Set(-3.0f, i);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E3_S3 = Timer_GetElapsed(g_time);
        }            
    }
    break;
    case ENEMY_FIGHTER_404_ERROR :
    if ((Timer_GetElapsed(g_time) - self->Timer_E1_M1 >=1)&&(Timer_GetElapsed(g_time) - self->Timer_E1_M1 < 3))
    {
    Vec2 velocity = Vec2_Set(0.0f, 1.0f);
    self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
    self->position, // (vitesse * temps écoulé)
    Vec2_Scale(velocity, Timer_GetDelta(g_time)*10.0f));
    }
    else if ((Timer_GetElapsed(g_time) - self->Timer_E1_M1 >= 0)&&(Timer_GetElapsed(g_time) - self->Timer_E1_M1 < 1))
    {
        Vec2 velocity = Vec2_Set(0.0f, -1.0f);
        self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
        self->position, // (vitesse * temps écoulé)
        Vec2_Scale(velocity, Timer_GetDelta(g_time)*10.0f));
    }
    else if ((Timer_GetElapsed(g_time) - self->Timer_E1_M1 >= 3)&&(Timer_GetElapsed(g_time) - self->Timer_E1_M1 < 4))
    {
        Vec2 velocity = Vec2_Set(0.0f, -1.0f);
        self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
        self->position, // (vitesse * temps écoulé)
        Vec2_Scale(velocity, Timer_GetDelta(g_time)*10.0f));
    }
    else
    {
        self->Timer_E1_M1 = (int)Timer_GetElapsed(g_time);
    }
    if (Timer_GetElapsed(g_time) - self->Timer_E1_S1 > 1)
    {
            Vec2 velocity = Vec2_Set(-10.0f, 0.0f);
            Bullet *bullet = Bullet_New(
            self->scene, self->position, velocity, BULLET_FIGHTER_404, 0.0f);
            Scene_AppendBullet(self->scene, bullet);
            self->Timer_E1_S1 = Timer_GetElapsed(g_time);          
    }
    }
}

void Enemy_Render(Enemy *self)
{
    // On récupère des infos essentielles (communes à tout objet)
    Scene *scene = self->scene;
    SDL_Renderer *renderer = Scene_GetRenderer(scene);
    Camera *camera = Scene_GetCamera(scene);
// On calcule la position en pixels en fonction de la position
// en tuiles, la taille de la fenêtre et la taille des textures.
    float scale = Camera_GetWorldToViewScale(camera);
    SDL_FRect dst = { 0 };
// Changez 48 par une autre valeur pour grossir ou réduire l'objet
    dst.h = 32 * PIX_TO_WORLD * scale;
    dst.w = 32 * PIX_TO_WORLD * scale;
    Camera_WorldToView(camera, self->position, &dst.x, &dst.y);
// Le point de référence est le centre de l'objet
    dst.x -= 0.50f * dst.w;
    dst.y -= 0.50f * dst.h;
// On affiche en position dst (unités en pixels)
    SDL_RenderCopyExF(
            renderer, self->texture, NULL, &dst, 0.0f, NULL, 0);
}

void Enemy_Damage(Enemy *self, int damage)
{
    switch (self->type)
    {
    case ENEMY_FIGHTER:
    self->lives -= damage;
    if (self->lives <= -8) {
        self->state = ENEMY_DEAD;
        playSound("destroyed");
    } else {
        playSound("touched");
    }
    break;
    case ENEMY_FIGHTER_404:
    self->lives -= damage;
    if (self->lives <= -14) {
        self->lives = 0;
        self->type = ENEMY_FIGHTER_404_ERROR;
        playSound("touched");
    } else {
        playSound("touched");
    }
    printf("%d\n", damage);
    break;
    case ENEMY_FIGHTER_MINION_1:
    self->lives -= damage;
    if (self->lives <= -2) {
        self->state = ENEMY_DEAD;
        playSound("destroyed");
    } else {
        playSound("touched");
    }
    printf("%d\n", damage);
    break;
    case ENEMY_FIGHTER_MINION_2:
    self->lives -= damage;
    if (self->lives <= -2) {
        self->state = ENEMY_DEAD;
        playSound("destroyed");
    } else {
        playSound("touched");
    }
    printf("%d\n", damage);
    break;
    case ENEMY_FIGHTER_404_ERROR:
    self->lives -= damage;
    if (self->lives <= -4) {
        self->state = ENEMY_DEAD;
        playSound("destroyed");
    } else {
        playSound("touched");
    }
    printf("%d\n", damage);
    break;
    }
}