#include "Text.h"
#include "Settings.h"
#include "Scene.h"

void Text_New(Scene *scene, char message[20], int x, int y, int rectWidth, int rectHeight, int fontSize, int r, int g, int b) {
    SDL_Renderer *renderer = Scene_GetRenderer(scene);

    if(renderer == NULL){
        exit(EXIT_FAILURE);
    }

    // Load font and create text surface
    TTF_Font *font = TTF_OpenFont("../Assets/Fonts/font.ttf", fontSize);
    if(font == NULL){
        fprintf(stderr, "Error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

//    Set colors
    SDL_Color textColor = { r, g, b };
    SDL_Surface *textSurf = TTF_RenderText_Solid(font, message, textColor);
    if(textSurf == NULL){
        fprintf(stderr, "Error: %s\n", TTF_GetError());
        exit(EXIT_FAILURE);
    }

    // Create texture from surface and set position
    SDL_Texture *textTexture = SDL_CreateTextureFromSurface(renderer, textSurf);
    if(textTexture == NULL){
        fprintf(stderr, "Error: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    SDL_Rect textRect = {x, y, rectWidth, rectHeight};

//  Cleanup
    SDL_FreeSurface(textSurf);
    TTF_CloseFont(font);

//    Render Text
    SDL_RenderCopy(renderer, textTexture, NULL, &textRect);
}