#pragma once

#include "Settings.h"

typedef struct TextDialog_s
{
    SDL_Window *window;
} TextDialog;

/// @brief Init text dialog
/// @param window The SDL Window
void *Dialog_init(SDL_Window *window);

/// @brief Add text dialog to screen
/// @param title A title
/// @param text The text to show
void *Dialog_New(char title[20], char text[1000]);
