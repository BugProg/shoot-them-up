#include "Player.h"
#include "Scene.h"
#include "Common.h"
#include "Text.h"
#include "Sound.h"

Player *Player_New(Scene *scene, int lives, int score, int playerId, int multiplayerMod)
{
    Player *self = (Player *)calloc(1, sizeof(Player));
    AssertNew(self);

    Assets *assets = Scene_GetAssets(scene);

    self->scene = scene;
    if (playerId == 1) {
        self->position = Vec2_Set(1.0f, 4.5f);
        self->texture = assets->player1;
    } else {
        if (multiplayerMod == 1) {
            self->texture = assets->player2Enemy;
            self->position = Vec2_Set(14.0f, 7);
        } else {
            self->texture = assets->player2;
            self->position = Vec2_Set(1.0f, 7);
        }
        self->multiplayerMod = multiplayerMod;
    }
    self->radius = 0.25f;
    self->lives = lives;
    self->score = score;
    self->playerId = playerId;
    self->timeMemoryShoot = 0;
    self->isGunReload = true;

    return self;
}

void Player_Delete(Player *self)
{
    if (!self) return;
    free(self);
}

void Player_Update(Player *self)
{
    if (self->state == PLAYER_DEAD) return;
    // On récupère des infos essentielles (communes à tout objet)
    Scene *scene = self->scene;
    Input *input = Scene_GetInput(scene);
// Mise à jour de la vitesse en fonction de l'état des touches
    Vec2 velocity;

    if (self->playerId == 1) {
        velocity = Vec2_Set(input->hAxis, input->vAxis);
    } else {
        velocity = Vec2_Set(input->hAxis2, input->vAxis2);
    }
    if(((input->livesPressed && self->playerId == 1) && Timer_GetElapsed(g_time) - self->Timer_ExtraLives > 1))
    {
        self->lives += 10;
        self->Timer_ExtraLives = Timer_GetElapsed(g_time);
    }
    if(((input->livesPressed && self->playerId == 2) && Timer_GetElapsed(g_time) - self->Timer_ExtraLives2 > 1)&& self->multiplayerMod == 0)
    {
        self->lives += 10;
        self->Timer_ExtraLives2 = Timer_GetElapsed(g_time);
    }
    if(((input->livesEnemyPressed && self->playerId == 2) && Timer_GetElapsed(g_time) - self->Timer_ExtraLives2 > 1)&& self->multiplayerMod == 1)
    {
        self->lives += 10;
        self->Timer_ExtraLives2 = Timer_GetElapsed(g_time);
    }

// Mise à jour de la position
    self->position = Vec2_Add( // Nouvelle pos. = ancienne pos. +
            self->position, // (vitesse * temps écoulé)
            Vec2_Scale(velocity, Timer_GetDelta(g_time)*3));

    if (!self->isGunReload) {
        self->isGunReload = true;
        playSound("reload");
    }

    if (((input->shootPressed && self->playerId == 1) || (input->shootPressed_Player2 && self->playerId == 2))
    && (Timer_GetElapsed(g_time) - self->timeMemoryShoot) > 1) {
        Vec2 velocity = Vec2_Set(self->multiplayerMod == 1 ? -7.0f : 7.0f, 0.0f);

//      First Gun
        Vec2 VecBullet1_Player = Vec2_Set( 0, -0.2f);
        Bullet *bullet1 = Bullet_New(
        self->scene,
        Vec2_Add(self->position, VecBullet1_Player),
        velocity,
        self->multiplayerMod == 1 ? BULLET_FIGHTER_PLAYER :BULLET_PLAYER,
        90.0f);
        Scene_AppendBullet(self->scene, bullet1);

//        Second gun
        Vec2 VecBullet2_Player = Vec2_Set( 0, 0.2f);
        Bullet *bullet2 = Bullet_New(
        self->scene,
        Vec2_Add(self->position, VecBullet2_Player),
        velocity,
        self->multiplayerMod == 1 ? BULLET_FIGHTER_PLAYER : BULLET_PLAYER,
        90.0f);
        Scene_AppendBullet(self->scene, bullet2);
        self->timeMemoryShoot = Timer_GetElapsed(g_time); // Update Timer memory
        playSound("shoot");
        self->isGunReload = false;
    }

    if ((input->shootUltimatePressed && self->playerId == 1 || input->shootUltimatePressed_Player2 && self->playerId == 2)
    && (Timer_GetElapsed(g_time) - self->timeMemoryUltimateShoot) > 3 && self->multiplayerMod == 0) {
        Vec2 velocity = Vec2_Set(self->multiplayerMod == 1 ? -10.0f : 10.0f, 0.0f);

//      Ultimate Gun
        Vec2 VecBulletUltimate_Player = Vec2_Set( 0, 0);
        Bullet *bulletultimate = Bullet_New(
        self->scene,
        Vec2_Add(self->position, VecBulletUltimate_Player),
        velocity, self->multiplayerMod == 1 ? BULLET_FIGHTER_ULTIMATE_PLAYER : BULLET_ULTIMATE_PLAYER,
        90.0f);
        Scene_AppendBullet(self->scene, bulletultimate);

        if (Timer_GetElapsed(g_time) - self->timeMemoryUltimateShoot > 3)
        { self->timeMemoryUltimateShoot = Timer_GetElapsed(g_time); // Update Timer memory
        } else { self->timeMemoryUltimateShoot_Player2 = Timer_GetElapsed(g_time); // Update Timer memory
        }
        playSound("big_shoot");
        self->isGunReload;
    }
}

void Player_Render(Player *self)
{
    if (self->state == PLAYER_DEAD) return;
// On récupère des infos essentielles (communes à tout objet)
    Scene *scene = self->scene;
    SDL_Renderer *renderer = Scene_GetRenderer(scene);
    Camera *camera = Scene_GetCamera(scene);
// On calcule la position en pixels en fonction de la position
// en tuiles, la taille de la fenêtre et la taille des textures.
    float scale = Camera_GetWorldToViewScale(camera);
    SDL_FRect dst = { 0 };
// Changez 48 par une autre valeur pour grossir ou réduire l'objet
    dst.h = 24 * PIX_TO_WORLD * scale;
    dst.w = 24 * PIX_TO_WORLD * scale;
    Camera_WorldToView(camera, self->position, &dst.x, &dst.y);
// Le point de référence est le centre de l'objet
    dst.x -= 0.50f * dst.w;
    dst.y -= 0.50f * dst.h;
// On affiche en position dst (unités en pixels)
    SDL_RenderCopyExF(
            renderer, self->texture, NULL, &dst, 0.0f, NULL, 0);
}

void Player_Damage(Player *self, int damage)
{
    if (self->state != PLAYER_IMMUNITY && self->state != PLAYER_DEAD) {
        self->lives -= damage;
        playSound("touched");
        self->state = PLAYER_IMMUNITY;
        if (self->playerId == 1 || ( self->playerId == 2 && self->multiplayerMod == 0)) {
            playSound("immunity");
        }
        self->livesTimeMemory = Timer_GetElapsed(g_time);
        if (self->lives <= 0) {
            self->state = PLAYER_DEAD;
            playSound("destroyed");
            self->position = Vec2_Set(1000.0f, 1000.0f);
        }
    }
    printf("Le potooship allié a mal : %d\n", self->lives);
}