#pragma once

#include "Settings.h"
#include "Math.h"

typedef struct Scene_s Scene;

/// @brief Enum�ration repr�sentant les �tats possibles du joueur.
typedef enum PlayerState_e
{
    /// @brief Etat par d�faut du joueur.
    PLAYER_FLYING,

    /// @brief Etat du joueur quand il joue l'animation de destruction.
    PLAYER_DYING,

    /// @brief Etat du joueur quand il est mort et que son animation de
    /// destruction est termin�e.
    PLAYER_DEAD,

    /// @brief State of the player when it can't lose lives.
    PLAYER_IMMUNITY,
} PlayerState;

/// @brief Enum�ration des types possibles pour un joueur.
typedef enum PlayerType_e
{
    /// @brief Joueur classe UBUNTU.
    PLAYER_UBUNTU,
    /// @brief Joueur classe DEBIAN.
    PLAYER_DEBIAN,
    /// @brief Joueur classe WINDOWS.
    PLAYER_WINDOWS,
} PlayerType;

/// @brief Structure repr�sentant le joueur.
typedef struct Player_s
{
    /// @brief Sc�ne associ�e.
    Scene *scene;

    /// @brief Texture utilis�e pour le rendu.
    SDL_Texture *texture;

    /// @brief Position du joueur exprim�e dans le r�f�rentiel monde.
    Vec2 position;

    /// @brief Etat du joueur.
    /// Les valeurs possibles sont d�finies dans PlayerState.
    int state;

    /// @brief Type du joueur.
    /// Les valeurs possibles sont d�finies dans PlayerType.
    int type;

    // @brief Contain the last state of the player.
    int stateMemory;

    /// @brief Rayon du joueur exprim� dans le r�f�rentiel monde.
    /// Il est utilis� dans le moteur physique pour tester les collisions.
    float radius;

    /// @brief Number of player's lives
    int lives;

    /// @brief Timer memory for lives;
    /// Updated when the player is touched by a bullet
    float livesTimeMemory;

    /// @brief Timer memory for bullets.
    /// To avoid spam bullets
    float timeMemoryShoot;

    /// @brief Timer memory for ultimate bullets.
    /// To avoid spam bullets
    float timeMemoryUltimateShoot;
    float timeMemoryUltimateShoot_Player2;

    float Timer_ExtraLives;

    float Timer_ExtraLives2;

    /// @brief the multiplayer game mod.
    /// 0 -> enemy mod
    /// 1 -> ally mod
    int multiplayerMod;

    ///@brief player score
    int score;

    ///@brief Player id for multiplayer
    int playerId;

    ///@brief reload play memory
    bool isGunReload;
} Player;

/// @brief Cr�e un nouveau joueur.
/// @param scene la sc�ne.
/// @return Le joueur cr��.
Player *Player_New(Scene *scene, int lives, int score, int playerId, int multiplayerMod);

/// @brief D�truit un joueur.
/// @param self le joueur.
void Player_Delete(Player *self);

/// @brief Met � jour le joueur.
/// @param self le joueur.
void Player_Update(Player *self);

/// @brief Dessine le joueur dans le moteur de rendu.
/// @param self le joueur.
void Player_Render(Player *self);

/// @brief Inflige des dommages au joueur.
/// @param self le joueur.
/// @param damage la quantit� de dommages (nombre de points de vie � perdre).
void Player_Damage(Player *self, int damage);
